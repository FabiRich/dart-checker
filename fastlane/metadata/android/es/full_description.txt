Dart Checker se encarga de su puntuación, mientras está jugando a los dardos
solo o con amigos. Es la mejor forma de hacer un marcador. No hace falta más cálculos 
manuales. Sólo pon tus dardos, como puedes hacer.

    Características

    * simple X01 (301, 501), modo de entrenamiento SET/LEG y FREE (puntuación variable), (CRAZY) CRICKET, ELIMINATION, HALVE IT
    * 1-8 jugadores (excepto en el modo SET/LEG)
    * maestro, doble y sencillo
    * sugerencias de checkout
    * deshacer múltiple en una fila de entradas mal hechas
    * estadísticas (sobre cuántos partidos y jugadores deseas)
    * dos variantes de entrada (dardo por dardo o puntuación total de tres dardos)
    * tema oscuro de ahorro de energía o uno claro
    * idioma: alemán / inglés / español / francés
    * offline - no se necesita nada de conexión a internet, aparte de la descarga de aplicaciones ;-)