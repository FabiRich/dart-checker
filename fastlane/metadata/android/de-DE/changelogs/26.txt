hinzugefügt:
- neuer Spieltyp: HALVE IT (schau in die Hilfe für die Spielregeln)
- Elimination: Anzeige der Restpunkte

repariert:
- Hauptmenü: Geisterschaltfläche ist aktiv während der Punkteingabe im FREE Modus
- Hauptmenü: Unmöglichkeit das Menü zu schließen, ohne einen Menüpnukt aufzurufen
- Hauptmenü: Schwierigkeit die FREE Punkteeingabe zu verlassen, ohne die Tastatur zu verwenden
- App-Beschreibung
- Hilfetexte