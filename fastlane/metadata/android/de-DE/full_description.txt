Dart Checker kümmert sich um die Punkte, während du allein oder mit Freunden Dart spielst.
Es ist die bessere Form einer Punktetafel. Es werden keine händischen Berechnungen mehr benötigt.
Tippe deine Treffer einfach so ein, wie du sie jemandem sagen würdest.

    Features

    * Spiele: einzelnes X01 (301, 501), SET/LEG, FREE Trainingsmodus (frei wählbare Punkteanzahl), (CRAZY) CRICKET, ELIMINATION, HALVE IT
    * 1-8 Spieler (ausgenommen SET/LEG Modus)
    * master, double und single out
    * master, double and single in
    * checkout-Vorschläge
    * mehrfache Rücknahme in Folge von fehlerhaften Eingaben
    * Statistiken (über beliebig viele Spiele und Spieler)
    * zwei Eingabevarianten (Dart für Dart oder gesamte Aufnahme)
    * energiesparendes, dunkles Design oder ein helles
    * Sprache: Deutsch, Spanisch, Englisch oder Französisch
    * offline - es wird absolut keine Internetverbindung benötigt oder genutzt, von dem eigentlichen App-Download abgesehen ;-)