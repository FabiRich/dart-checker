added:
- preferences: start of crazy cricket range

fixed:
- cricket: app freeze at end of match, when option "the game continues after the first player has won" is enabled
- cricket: display of wrong information when denying question on last throw